# azrs-tracking

Ovaj repozitorijum služi kao prikaz primene alata za razvoj softvera na projektu "Studysphere". Projekat je realizovan kao deo kursa "Alati za razvoj softvera" na Matematičkom fakultetu.

[Link do StudySphere projekta](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/StudySphere)

## Korišćeni alati

1.[Git](https://gitlab.com/jjulijana/azrs-tracking/-/issues/1)

2.[Meld](https://gitlab.com/jjulijana/azrs-tracking/-/issues/4)

3.[ClangFormat](https://gitlab.com/jjulijana/azrs-tracking/-/issues/5)

4.[CMake](https://gitlab.com/jjulijana/azrs-tracking/-/issues/6)

5.[Docker](https://gitlab.com/jjulijana/azrs-tracking/-/issues/7)

6.[CI](https://gitlab.com/jjulijana/azrs-tracking/-/issues/8)

7.[ClangTidy](https://gitlab.com/jjulijana/azrs-tracking/-/issues/9)

8.[Git hook](https://gitlab.com/jjulijana/azrs-tracking/-/issues/10)

9.[Doxygen](https://gitlab.com/jjulijana/azrs-tracking/-/issues/11)

10.[Skript jezici](https://gitlab.com/jjulijana/azrs-tracking/-/issues/13)
